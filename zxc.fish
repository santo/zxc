#! /usr/bin/env fish
# set params 'h/help' 't/type=' 'n/name=?' 's/symptom=?' 'c/composition=?' 'd/detail=?' 'r/reindex' 'v/verbose=?' 
# set description 'display help message' 'data type, avaluable type: fang/yao' 'name of fang/yao, usually a heading' 'symptom, usually a quote' 'composition, usually a list' 'detail, this is a full-text search' 'after modify the file, you should redinex it.' 'verbose decide how many infomation was shown' 
set params 'h/help' 'k/keyword=' 'r/reindex' 't/type='
set types 'ym:药名' 'fm:方名' 'zz:症状' 'zf:组分' 'fl:方论' 'yj:药解' 
argparse -n zxc $params -- $argv
or return

if [ -n "$_flag_h" ]
    set description '显示本帮助'  '查询关键词' '重建索引，当文本数据修改时，应当删除或重建索引' '查询类别'
    echo (status basename) 'Usage:'
    for i in (seq  (count $params))
        printf \t-%s\t\t%s\n (string replace '/' '/--' $params[$i]) $description[$i]
    end
    printf '\n\t查询类别可用选项：\n'
    printf \t\t%s\n $types
    exit
end

# --------------file info prepare
function fselect
    string split ' ' "$argv" | fzf --reverse --ansi --query "" --preview "" --prompt '请选择：'
end

function getType
    if [ -z "$_flag_t" ] 
        set -f _t (fselect $types |cut -d: -f1 )
        # fselect $types |cut -d: -f1
        # echo (count $_t)
        # echo $_t
        if test -z "$_t"
            printf "已取消"
            exit 1
        end
        echo $_t
    else
        # set _t $_flag_t
        echo $_flag_t
    end
end

function getDocs
    # if [ "$_t" = 'ym:药名' -o "$_t" = 'yj:药解' ]
    if string match -qr '^y' "$argv[1]"
        set -f doc (status dirname)'/yaojie.md'
    else
        set -f doc (status dirname)'/fangji.md'
    end
    echo $doc

    # set -f indexPath (dirname $doc)/.(basename -s .md $doc).index
    echo (dirname $doc)/.(basename -s .md $doc).index
    # echo $doc $indexPath
end

function reIndex
    set -f _docs $argv
    # echo received $argv (count $argv)
    # echo Buiding index for $_docs[1]: $_docs[2]
    sed -nr '/^#+ /{=;p}' $_docs[1] > $_docs[2]
end
#------------------------------------

#----------keyword-------------
function getKey
    if [ -z "$_flag_k" ] 
        read -P "> 查询关键词：" _k
        if test -z "$_k"
            printf "> 已取消"
            return 1
        end
        echo $_k
    else
        # set _k $_flag_k
        echo $_flag_k
    end
    # echo $_k
end

#-----------search------------
function doSearch -a _t _k doc
    # echo dosearch: type=$_t key=$_k doc=$doc
    if string match -qr '^\wm' "$_t" 
        # ym or fm
        # echo 'searching name'
        # set -x list "$(sed -n "/^### .*$_k/{N;N;N;N;p}" $doc)"
        sed -n "/^### .*$_k/{N;N;N;N;p}" $doc|string collect
        # set prevcmd 'rg --column --line-number --no-heading --color=always --smart-case -A15 -m1 {} $doc'
    else if string match -qr "^zz" $_t 
        # echo 'searching quote'
        # set -x list "$(sed -n "/^### /{h;n}; /^> .*$_k/{N;N;N;H;g;p}" $doc)"
        sed -n "/^### /{h;n}; /^> .*$_k/{N;N;N;H;g;p}" $doc |string collect
    else if string match -qr "^zf" $_t 
        # echo 'searching list'
        # sed -n '/^### /{N;N;N;h}; /^- .*石膏/,/^\s*$/{H;g;p}'
        # set -x list "$(sed -n "/^### /{N;N;N;N;h}; /^- .*$_k/{N;N;H;g;p}" $doc)"
        sed -n "/^### /{N;N;N;N;h}; /^- .*$_k/{N;N;H;g;p}" $doc | string collect
    else
        # fl or yj
        # echo 'searching fulltext'
        # set -x list "$(sed -n "/^### /{N;N;N;N;h}; /$_k/{N;H;g;p}" $doc)"
        sed -n "/^### /{N;N;N;N;h}; /$_k/{N;H;g;p}" $doc | string collect
    end
    # echo $list
end

function doSelect
    set -x list $argv
    if [ -z "$list" ]
        echo 'No Records to select'
        return 1
    end
    set list_head "$(printf $list | sed -n '/^### / s/\s*$// p' |uniq)"
    set list_count  (echo $list_head | wc -l)
    # echo list: $list
    # echo head: $list_head
    # (count $list_head)
    # echo "found:" $list_count

    if [ "$list_count" -gt 1 ]
        set prevcmd 'echo $list | rg --column --line-number --no-heading --color=always --smart-case -A15 -m1 {}'
        # set res "$( echo $list_head | fzf --reverse --ansi --query "" \
        #     --preview "$prevcmd" \
        echo $list_head | fzf --reverse --ansi --query ""  --preview "$prevcmd" --preview-window "right:65%" --prompt "您需要查询的是："
    else
        echo $list_head
    end

    # echo your select is $choosed
    # echo your select is \"$res\"
end

function findIndex -a title 
    [ -f "$indexPath" ] || reIndex
    if [ -z "$title" ]
        echo "nothing selected" >&2
        exit 1
    end
    sed -n "/^[0-9]/h; /$title/{n;H;;g;p;q}" $indexPath
    # echo index: $indexes[1], $indexes[2]
    # echo $indexes
end

function viewByIndex
    set indexes (string split ' ' $argv)
    # echo $argv index:$indexes[1] index2: $indexes[2]
    if test (count "$indexes") -lt 1
        printf '未找到'
        exit 2
    end
    sed -n "$indexes[1],$(math $indexes[2] - 1) {p}" $doc
end

function view -a title
    if [ -n "$title" ]
        viewByIndex (findIndex $title)
    else
        echo "nothing selected."
        return 2
    end
end

function main
    set _t (getType)
    echo 
    echo '> 查询类别：' $_t
    set _docs (getDocs $_t)
    set -g doc $_docs[1]
    set -g indexPath $_docs[2]

    if [ -n "$_flag_r" ]
        reIndex $_docs
        exit
    end
    set _k (getKey)
    set list (doSearch $_t $_k $_docs[1])
    and set title (doSelect "$list")
    # echo selected:$selected--
    #         set index (findIndex $title)
    #         echo $index
    #     viewByIndex $index
    and if type -q bat
        view $title | bat -l md --paging always -
    else
      echo (view $title)
    end
    or echo '> 未找到关键词：' $_k '相关内容'
    echo

end

echo "
#########################################################################

                            张锡纯医学衷中参西录
                                快速查询程式

#########################################################################
                        https://codeberg.org/santo/zxc

"
if test -n "$_flag_t"
    main
else
    echo "如需退出请按 Esc 或 Ctrl+c"
    while true
        main
    end
end



