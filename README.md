#########################################################################

                            张锡纯医学衷中参西录
                                快速查询程式

#########################################################################

# 用途
快速查询《医学衷中参西录》中关于方剂，药草的相关记载。

# 用法
下载后，执行k`zxc.fish`

# dependences

- fish shell
- sed
- fzf
- bat

only for Linux, maybe WSL works in windows.

# 效果图

![](https://raw.codeberg.page/santo/zxc/imgs/zxc1.png)

![](https://raw.codeberg.page/santo/zxc/imgs/zxc2.png)

![](https://raw.codeberg.page/santo/zxc/imgs/zxc3.png)

# 参数说明 
zxc.fish Usage:

	-h/--help		显示本帮助
	
	-k/--keyword=		查询关键词
	
	-r/--reindex		重建索引，当文本数据修改时，应当删除或重建索引
	
	-t/--type=		查询类别

	查询类别可用选项：
	
		ym:药名
		fm:方名
		zz:症状
		zf:组分
		fl:方论
		yj:药解

    
